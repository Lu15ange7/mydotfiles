# MY DOT FILES

![MyDotFiles](https://gitlab.com/Lu15ange7/mydotfiles/-/raw/main/preview.png "MyDotFiles")

My files for customizer my desktop (using i3 like a window manager)

## Requeriment:
- `i3`
- `Polybar`
- `Rofi`
- `Picom`
- `Neofetch`
- `ttf-siji`
- `Feh`
- `xfce4-notifyd`
- `xfce4-power-manager`
- `slimbookbatteryindicator`
- `slimbookintelcontrollerindicator`
- `Oh my bash`

## Install:
install all Requeriments
starting first with Oh my bash
```
bash -c "$(curl -fsSL https://raw.githubusercontent.com/ohmybash/oh-my-bash/master/tools/install.sh)"
```
Now We can install these dotfiles
```
make install
```
## Uninstall:
```
make uninstall
```

# Thanks you

Launchpad Rofi made by [lr-tech](https://github.com/lr-tech/ "lr-tech"), source: [Github](https://github.com/lr-tech/rofi-themes-collection "Github")

Wallpaper made by [odce1206](https://www.reddit.com/user/odce1206/ "odce1206"), source: [Reddit](https://www.reddit.com/r/archlinux/comments/4ezi5p/i_just_made_this_wallpaper_using_the_arch_logo/ "Reddit")

Oh My BASH! made by [Toan Nguyen](https://nntoan.com "Toan Nguyen"), source: [Oh My BASH! Website](https://ohmybash.nntoan.com "Oh My BASH! Website")
