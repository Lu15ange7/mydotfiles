all: install

welcome:
	@echo '										'
	@echo '---------------------------------------------------------------------------------------'
	@echo '										'
	@echo ' __   __  __   __  ______   _______  _______  _______  ___   ___      _______  _______ '
	@echo '|  |_|  ||  | |  ||      | |       ||       ||       ||   | |   |    |       ||       |'
	@echo '|       ||  |_|  ||  _    ||   _   ||_     _||    ___||   | |   |    |    ___||  _____|'
	@echo '|       ||       || | |   ||  | |  |  |   |  |   |___ |   | |   |    |   |___ | |_____ '
	@echo '|       ||_     _|| |_|   ||  |_|  |  |   |  |    ___||   | |   |___ |    ___||_____  |'
	@echo '| ||_|| |  |   |  |       ||       |  |   |  |   |    |   | |       ||   |___  _____| |'
	@echo '|_|   |_|  |___|  |______| |_______|  |___|  |___|    |___| |_______||_______||_______|'
	@echo '										'
	@echo '		  My files for customizer my desktop (using i3 like a window manager)'
	@echo '										'
	@echo '---------------------------------------------------------------------------------------'
	@echo '										'


install: welcome
	@cp -r .config ~
	@cp -r .local ~
	@cp -r wallpaper ~
	@mkdir ~/dbus-1
	@mkdir ~/dbus-1/services
	@cp org.freedesktop.Notifications.service ~/dbus-1/services
	@cp .bashrc ~
	@echo " ✓ MyDotFiles installed"
	@echo '										'
	@echo '---------------------------------------------------------------------------------------'
	@echo '										'


uninstall: welcome
	@rm -r ~/.config/i3
	@rm -r ~/.config/picom
	@rm -r ~/.config/polybar
	@rm -r ~/.config/neofetch
	@rm -r ~/wallpaper/archwallpaper.jpg
	@rm -r ~/.config/rofi
	@rm -r ~/.local/share/rofi
	@echo " ✓ MyDotFiles uninstalled"
	@echo '										'
	@echo '---------------------------------------------------------------------------------------'
	@echo '										'
